package main

import (
	"github.com/tadvi/winc"
)

var mainWindow = winc.NewForm(nil)

var joueur bool = true
var Play [10]bool //9+1
var PlayG [10]int //9+1

var GG = winc.NewEdit(mainWindow)

var btn1 = winc.NewPushButton(mainWindow)
var btn2 = winc.NewPushButton(mainWindow)
var btn3 = winc.NewPushButton(mainWindow)
var btn4 = winc.NewPushButton(mainWindow)
var btn5 = winc.NewPushButton(mainWindow)
var btn6 = winc.NewPushButton(mainWindow)
var btn7 = winc.NewPushButton(mainWindow)
var btn8 = winc.NewPushButton(mainWindow)
var btn9 = winc.NewPushButton(mainWindow)

func main() {

	mainWindow.SetSize(350, 250)
	mainWindow.SetText("Morpion DEROGIS BARBEAU CHRISTOPHE B2G2")

	btn1.SetText("+")
	btn1.SetPos(10, 20)
	btn1.SetSize(20, 20)
	btn1.OnClick().Bind(func(e *winc.Event) {
		if Play[1] == false {
			if joueur == true {
				btn1.SetText("X")
				joueur = false
				PlayG[1] = 1
			} else {
				btn1.SetText("O")
				joueur = true
				PlayG[1] = 2
			}
			Play[1] = true
		}
		iswin()
	})

	btn2.SetText("+")
	btn2.SetPos(40, 20)
	btn2.SetSize(20, 20)
	btn2.OnClick().Bind(func(e *winc.Event) {
		if Play[2] == false {
			if joueur == true {
				btn2.SetText("X")
				joueur = false
				PlayG[2] = 1
			} else {
				btn2.SetText("O")
				joueur = true
				PlayG[2] = 2
			}
			Play[2] = true
		}
		iswin()
	})

	btn3.SetText("+")
	btn3.SetPos(70, 20)
	btn3.SetSize(20, 20)
	btn3.OnClick().Bind(func(e *winc.Event) {
		if Play[3] == false {
			if joueur == true {
				btn3.SetText("X")
				joueur = false
				PlayG[3] = 1
			} else {
				btn3.SetText("O")
				joueur = true
				PlayG[3] = 2
			}
			Play[3] = true
		}
		iswin()

	})

	btn4.SetText("+")
	btn4.SetPos(10, 40)
	btn4.SetSize(20, 20)
	btn4.OnClick().Bind(func(e *winc.Event) {
		if Play[4] == false {
			if joueur == true {
				btn4.SetText("X")
				joueur = false
				PlayG[4] = 1
			} else {
				btn4.SetText("O")
				joueur = true
				PlayG[4] = 2
			}
			Play[4] = true
		}
		iswin()

	})

	btn5.SetText("+")
	btn5.SetPos(40, 40)
	btn5.SetSize(20, 20)
	btn5.OnClick().Bind(func(e *winc.Event) {
		if Play[5] == false {
			if joueur == true {
				btn5.SetText("X")
				joueur = false
				PlayG[5] = 1
			} else {
				btn5.SetText("O")
				joueur = true
				PlayG[5] = 2
			}
			Play[5] = true
		}
		iswin()
	})

	btn6.SetText("+")
	btn6.SetPos(70, 40)
	btn6.SetSize(20, 20)
	btn6.OnClick().Bind(func(e *winc.Event) {
		if Play[6] == false {
			if joueur == true {
				btn6.SetText("X")
				joueur = false
				PlayG[6] = 1
			} else {
				btn6.SetText("O")
				joueur = true
				PlayG[6] = 2
			}
			Play[6] = true
		}
		iswin()
	})

	btn7.SetText("+")
	btn7.SetPos(10, 60)
	btn7.SetSize(20, 20)
	btn7.OnClick().Bind(func(e *winc.Event) {
		if Play[7] == false {
			if joueur == true {
				btn7.SetText("X")
				joueur = false
				PlayG[7] = 1
			} else {
				btn7.SetText("O")
				joueur = true
				PlayG[7] = 2
			}
			Play[7] = true
		}
		iswin()
	})

	btn8.SetText("+")
	btn8.SetPos(40, 60)
	btn8.SetSize(20, 20)
	btn8.OnClick().Bind(func(e *winc.Event) {
		if Play[8] == false {
			if joueur == true {
				btn8.SetText("X")
				joueur = false
				PlayG[8] = 1
			} else {
				btn8.SetText("O")
				joueur = true
				PlayG[8] = 2
			}
			Play[8] = true
		}
		iswin()
	})

	btn9.SetText("+")
	btn9.SetPos(70, 60)
	btn9.SetSize(20, 20)
	btn9.OnClick().Bind(func(e *winc.Event) {

		if Play[9] == false {
			if joueur == true {
				btn9.SetText("X")
				joueur = false
				PlayG[9] = 1
			} else {
				btn9.SetText("O")
				joueur = true
				PlayG[9] = 2
			}
			Play[9] = true
		}
		iswin()
	})

	GG.SetPos(20, 100)
	GG.SetSize(100, 20)

	btn := winc.NewPushButton(mainWindow)
	btn.SetText("Fermer")
	btn.SetPos(20, 120)
	btn.SetSize(100, 40)
	btn.OnClick().Bind(wndOnClose)

	mainWindow.Show()
	winc.RunMainLoop()
}

func iswin() {

	if (PlayG[1] == 1 && PlayG[2] == 1 && PlayG[3] == 1) || // Winable condition
		(PlayG[4] == 1 && PlayG[5] == 1 && PlayG[6] == 1) ||
		(PlayG[7] == 1 && PlayG[8] == 1 && PlayG[9] == 1) ||
		(PlayG[1] == 1 && PlayG[4] == 1 && PlayG[7] == 1) ||
		(PlayG[2] == 1 && PlayG[5] == 1 && PlayG[8] == 1) ||
		(PlayG[3] == 1 && PlayG[6] == 1 && PlayG[9] == 1) ||
		(PlayG[1] == 1 && PlayG[5] == 1 && PlayG[9] == 1) ||
		(PlayG[3] == 1 && PlayG[5] == 1 && PlayG[7] == 1) {

		GG.SetText("Player 1 (X) Win")

	} else if (PlayG[1] == 2 && PlayG[2] == 2 && PlayG[3] == 2) || // Winable condition
		(PlayG[4] == 2 && PlayG[5] == 2 && PlayG[6] == 2) ||
		(PlayG[7] == 2 && PlayG[8] == 2 && PlayG[9] == 2) ||
		(PlayG[1] == 2 && PlayG[4] == 2 && PlayG[7] == 2) ||
		(PlayG[2] == 2 && PlayG[5] == 2 && PlayG[8] == 2) ||
		(PlayG[3] == 2 && PlayG[6] == 2 && PlayG[9] == 2) ||
		(PlayG[1] == 2 && PlayG[5] == 2 && PlayG[9] == 2) ||
		(PlayG[3] == 2 && PlayG[5] == 2 && PlayG[7] == 2) {

		GG.SetText("Player 2 (O) Win")
	}
}

func wndOnClose(arg *winc.Event) {
	winc.Exit()
}
